package Entities;

import javax.persistence.*;

/**
 * Created by hegerbra on 09/03/16.
 */



@Entity
@Table(name = "PersonAdress")
public class PersonAdress implements IFileEntity {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public PersonAdress(Person person, Adress adress) {
        this.person = person;
        this.adress = adress;
    }
    @OneToOne
    private Person person;

    @OneToOne
    private Adress adress;


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
