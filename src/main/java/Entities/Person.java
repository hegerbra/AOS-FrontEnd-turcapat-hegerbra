package Entities;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity
@Table(name = "Person")
public class Person implements IFileEntity {

    private static final long serialVersionUID = 6578545643567569L;
    private String firstName;
    private String lastName;


    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<PersonAdress> personAdresses =  new HashSet<PersonAdress>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<PersonContact> personContacts=  new HashSet<PersonContact>();

    @ManyToMany(cascade = CascadeType.ALL)
    private Collection<PersonVehicle> personVehicles=  new HashSet<PersonVehicle>();

    public Collection<PersonContact> getPersonContacts() {
        return personContacts;
    }

    public void setPersonContacts(HashSet<PersonContact> personContacts) {
        this.personContacts = personContacts;
    }

    public Collection<PersonVehicle> getPersonVehicles() {
        return personVehicles;
    }

    public void setPersonVehicles(HashSet<PersonVehicle> personVehicles) {
        this.personVehicles = personVehicles;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%-15s %-15s %-20s", this.getFirstName(),this.getLastName(),this.getId()));
        return String.format(sb.toString());
    }

    public Collection<PersonAdress> getPersonAdresses() {
        return personAdresses;
    }


    public void setPersonAdresses(HashSet<PersonAdress> personAdresses) {
        this.personAdresses = personAdresses;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

