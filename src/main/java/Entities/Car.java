package Entities;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "Car")
public class Car extends Vehicle   {

    public Car(){
        this.numberOfSeats = 5;
    }

    @Override
    public void makeNoise() {
        System.out.println("Brrm");
    }

}
