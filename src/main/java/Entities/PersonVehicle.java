package Entities;

import javax.persistence.*;

/**
 * Created by hegerbra on 09/03/16.
 */

@Entity
@Table(name = "PersonVehicle")
public class PersonVehicle implements IFileEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public PersonVehicle() {

    }

    private int personId;

    private int vehicleId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

 
}
