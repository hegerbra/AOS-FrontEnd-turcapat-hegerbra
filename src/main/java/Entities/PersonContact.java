package Entities;

import javax.persistence.*;

/**
 * Created by hegerbra on 09/03/16.
 */

@Entity
@Table(name = "PersonContact")
public class PersonContact implements IFileEntity {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    public PersonContact(Person person, Contact contact) {
        this.person = person;
        this.contact = contact;
    }

    @OneToOne
    private  Person person;

    @OneToOne
    private Contact contact;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
