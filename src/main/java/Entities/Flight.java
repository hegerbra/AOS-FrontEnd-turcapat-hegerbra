package Entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "flight")
@XmlRootElement
public class Flight {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @NotNull
    private Date dateOfDeparture;
    
    @ManyToOne
    @JoinColumn(name="from_destination_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Destination fromDestination;
    
    @ManyToOne
    @JoinColumn(name="to_destination_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Destination toDestination;

    @NotNull
    private String name;
    
    
    private Double distance;
    
    private Double price;
    
    private int seats;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="flight")
    private List<Reservation> reservation = new ArrayList<Reservation>();

    public Flight() {}

    
    public Flight( Date dateOfDeparture, Destination fromDestination, Destination toDestination, String name,
    		Double distance, Double price, int seats, List<Reservation> reservations) {
		this.dateOfDeparture = dateOfDeparture;
		this.fromDestination = fromDestination;
		this.toDestination = toDestination;
		this.name = name;
		this.distance = distance;
		this.price = price;
		this.seats = seats;
		this.reservation = reservations;
	}


	public Date getDateOfDeparture() {
		return dateOfDeparture;
	}


	public void setDateOfDeparture(Date dateOfDeparture) {
		this.dateOfDeparture = dateOfDeparture;
	}


	public Double getDistance() {
		return distance;
	}


	public void setDistance(Double distance) {
		this.distance = distance;
	}


	public Double getPrice() {
		return price;
	}


	public void setPrice(Double price) {
		this.price = price;
	}


	public int getSeats() {
		return seats;
	}


	public void setSeats(int seats) {
		this.seats = seats;
	}


	public Destination getFromDestination() {
		return fromDestination;
	}


	public void setFromDestination(Destination fromDestination) {
		this.fromDestination = fromDestination;
	}


	public Destination getToDestination() {
		return toDestination;
	}


	public void setToDestination(Destination toDestination) {
		this.toDestination = toDestination;
	}


	public List<Reservation> getReservations() {
		return reservation;
	}


	public void setReservations(List<Reservation> reservations) {
		this.reservation = reservations;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
