package Entities;

import java.lang.Thread.State;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "reservation")
@XmlRootElement
public class Reservation {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @NotNull
    private Date created;

    @NotNull
    private Integer seats;

    @NotNull
    private String password;
    
    @ManyToOne
    @JoinColumn(name="flight_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Flight flight;

    @NotNull
    private StateChoices state = StateChoices.NEW;

    public Reservation() {
    }

	public Reservation(Date created, Integer seats, String password, Flight flight, StateChoices state) {
		this.created = created;
		this.seats = seats;
		this.password = password;
		this.flight = flight;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Integer getSeats() {
		return seats;
	}

	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public StateChoices getState() {
		return state;
	}

	public void setState(StateChoices state) {
		this.state = state;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}


    
}
