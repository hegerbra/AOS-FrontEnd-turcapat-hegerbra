package Entities;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Truck")
public class Truck extends Vehicle {

     public Truck (){
         this.numberOfSeats = 2;
    }

    int transportAreaInSqrMeters;

    public int getTransportAreaInSqrMeters() {
        return transportAreaInSqrMeters;
    }

    public void setTransportAreaInSqrMeters(int transportAreaInSqrMeters) {
        this.transportAreaInSqrMeters = transportAreaInSqrMeters;
    }

    @Override
    public void makeNoise() {
        System.out.println("Vrrm");
    }

}
