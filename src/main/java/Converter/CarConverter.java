package Converter;

/**
 * Created by hegerbra on 23/05/16.
 */

import Entities.Car;
import Entities.PersonVehicle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;


public class CarConverter implements Converter{

    private final List<Car> CarList;

    public CarConverter(List<Car> CarList) {
        super();
        this.CarList = CarList;
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string) {

            int id = Integer.parseInt(string);

            for(Car person : CarList){
            	if(person.getId()==id){
            		return person;
            	}
            }

            return null;

            

    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Car Car = (Car) object;
        if (Car == null) {
            return null;
        } else {
            long id = Car.getId();
            return new Long(id).toString();
        }
    }

}