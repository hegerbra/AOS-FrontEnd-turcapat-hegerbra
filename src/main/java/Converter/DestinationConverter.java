package Converter;

/**
 * Created by hegerbra on 23/05/16.
 */

import Entities.Destination;
import Entities.PersonVehicle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;


public class DestinationConverter implements Converter {

    private final List<Destination> DestinationList;

    public DestinationConverter(List<Destination> DestinationList) {
        super();
        this.DestinationList = DestinationList;
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string) {

            int id = Integer.parseInt(string);

            for(Destination person : DestinationList){
            	if(person.getId()==id){
            		return person;
            	}
            }

            return null;


    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Destination Destination = (Destination) object;
        if (Destination == null) {
            return null;
        } else {
            long id = Destination.getId();
            return new Long(id).toString();
        }
    }

}