package Converter;

/**
 * Created by hegerbra on 23/05/16.
 */

import Entities.Person;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;


public class PersonConverter implements Converter {

    private final List<Person> PersonList;

    public PersonConverter(List<Person> PersonList) {
        super();
        this.PersonList = PersonList;
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string) {

            int id = Integer.parseInt(string);
            for(Person person : PersonList){
            	if(person.getId()==id){
            		return person;
            	}
            }

            return null;


    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        Person Person = (Person) object;
        if (Person == null) {
            return null;
        } else {
            long id = Person.getId();
            return new Long(id).toString();
        }
    }

}