package Converter;

import Entities.Person;

/**
 * Created by hegerbra on 23/05/16.
 */

import Entities.PersonVehicle;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.List;


public class PersonVehicleConverter implements Converter {

    private final List<PersonVehicle> PersonVehicleList;

    public PersonVehicleConverter(List<PersonVehicle> PersonVehicleList) {
        super();
        this.PersonVehicleList = PersonVehicleList;
    }

    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String string) {

            int id = Integer.parseInt(string);

            
            for(PersonVehicle person : PersonVehicleList){
            	if(person.getId()==id){
            		return person;
            	}
            }

            return null;


    }

    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object object) {
        PersonVehicle PersonVehicle = (PersonVehicle) object;
        if (PersonVehicle == null) {
            return null;
        } else {
            long id = PersonVehicle.getId();
            return new Long(id).toString();
        }
    }

}