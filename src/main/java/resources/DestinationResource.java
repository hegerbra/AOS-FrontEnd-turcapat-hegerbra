package resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.research.ws.wadl.HTTPMethods;

import Entities.Destination;

public class DestinationResource {

	String username = "user";
	String password = "user";

	String usernameAndPassword = username + ":" + password;
	String authorizationHeaderName = "Authorization";
	String authorizationHeaderValue = "Basic "
			+ java.util.Base64.getEncoder().encodeToString(usernameAndPassword.getBytes());

	private Gson gson;
	private WebTarget target;

	public DestinationResource(WebTarget t) {
		this.target = t;
		this.gson = new Gson();
	}
	
	


	public List<Destination> getAll() {

		Response response = target.path("destination").request()
				.header(authorizationHeaderName, authorizationHeaderValue).accept(MediaType.APPLICATION_JSON).get();
		String responseBody = response.readEntity(String.class);
		List<Destination> objects = null;

		if (response.getStatus() == 200) {
			Type collectionType = new TypeToken<Collection<Destination>>() {
			}.getType();
			objects = gson.fromJson(responseBody, collectionType);

		} else {
			System.out.println("Customer list is empty.");
		}
		return (List<Destination>) objects;
	}

	public void addDestination(Destination p) {
		String jsonObj = gson.toJson(p);
		Response response = target.path("destination").request()
				.header(authorizationHeaderName, authorizationHeaderValue).post(Entity.json(jsonObj));
		if (response.getStatus() == 201) {
			System.out.println("Success");
		} else {
			System.out.println("Unsuccessful");
		}
	}

	public void deleteDestination(Long id) {
		Response response = target.path("destination").path(String.valueOf(id)).request()
				.header(authorizationHeaderName, authorizationHeaderValue).delete();
		if (response.getStatus() == 201) {
			System.out.println("Success");
		} else {
			System.out.println("Unsuccessful");
		}
	}

	public void updateDestination(Destination p) {
		String jsonObj = gson.toJson(p);
		
		Long id = p.getId();
		String sId = String.valueOf(id);
		Response response = target.path("destination").path(sId).request()
				.header(authorizationHeaderName, authorizationHeaderValue).put(Entity.json(jsonObj));
		if (response.getStatus() == 201) {
			System.out.println("Success");
		} else {
			System.out.println("Unsuccessful");
		}
	}

}