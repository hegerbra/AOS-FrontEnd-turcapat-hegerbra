package resources;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import Entities.Person;
import Entities.PersonVehicle;



public class PersonResource {

	private Gson gson;
	private WebTarget target;
	
	public PersonResource(WebTarget t) {
		this.target = t;
		this.gson = new Gson();
	}
	
	public List<Person> getAll(){
		
		Response response = target.path("person").path("get").request().accept(MediaType.APPLICATION_JSON).get();
    	String responseBody = response.readEntity(String.class);
    	List<Person> persons = null;
	
	    if (response.getStatus() == 200) {
	    	Type collectionType = new TypeToken<Collection<Person>>(){}.getType();
	    	persons = gson.fromJson(responseBody, collectionType);
	    	  
	    } else {
	    	System.out.println("Customer list is empty.");
	    }
	    
	
	    
	    
	    return persons;
	}

	
	public void addPerson(Person p){

		
		String jsonObj = gson.toJson(p);
		Response response = target.path("person").path("post").request().post(Entity.json(jsonObj));
		 if (response.getStatus() == 200) {
			 System.out.println("Success");
		 } else {
			 System.out.println("Unsuccessful");
		 }
	}
}