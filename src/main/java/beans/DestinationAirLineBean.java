package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.EditableValueHolder;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.AjaxBehaviorEvent;
import javax.jms.JMSException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.glassfish.jersey.client.ClientConfig;
import org.richfaces.JsfVersion;

import Converter.CarConverter;
import Converter.DestinationConverter;

/**
 * Created by hegerbra on 21/05/16.
 */

import Converter.PersonConverter;
import Converter.PersonVehicleConverter;
import Entities.Car;
import Entities.Destination;
import Entities.Person;
import Entities.PersonVehicle;
import resources.CarResource;
import resources.DestinationResource;
import resources.PersonResource;
import resources.PersonVehicleResource;

public class DestinationAirLineBean {

	String url = "http://localhost:8080/aos-0.0.1-SNAPSHOT";

	ClientConfig config = new ClientConfig();

	Client client = ClientBuilder.newClient(config);

	WebTarget target = client.target(url);

	DestinationResource destinationResource = new DestinationResource(target);

	CarResource carResource = new CarResource(target);
	PersonResource personResource = new PersonResource(target);
	PersonVehicleResource personVehicleResource = new PersonVehicleResource(target);
	

	//// DESTINATION
	private Destination destination = new Destination();

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	private List<Destination> destinationList;

	public List<Destination> getDestinationList() {

		if (destinationList == null) {
			// carList = service.loadCars();
			destinationList = destinationResource.getAll();

		}

		return destinationList;
	}

	private DestinationConverter destinationConverter;

	public DestinationConverter destinationConverter() {
		if (destinationConverter == null) {
			destinationConverter = new DestinationConverter(getDestinationList());
		}
		return destinationConverter;
	}

	public String addDestination() {
		
		destinationList = null;
		destinationResource.addDestination(getDestination());

		return null;
	}

	public void deleteDestination(AjaxBehaviorEvent event) throws AbortProcessingException {
		UIParameter p = (UIParameter) event.getComponent().getChildren().get(0);
		destinationList = null;

		destinationResource.deleteDestination((Long) p.getValue());
	}
	
	public void updateDestination(AjaxBehaviorEvent event) throws AbortProcessingException {
		UIParameter p = (UIParameter) event.getComponent().getChildren().get(0);

		destinationResource.updateDestination( (Destination) p.getValue());
	}

	public void adddDestination(AjaxBehaviorEvent event) throws AbortProcessingException {
		UIParameter p = (UIParameter) event.getComponent().getChildren().get(0);

		destinationResource.addDestination( (Destination) p.getValue());
	}
	
	
	private int currentDestinationIndex;
	

	public int getCurrentDestinationIndex() {
		return currentDestinationIndex;
	}

	public void setCurrentDestinationIndex(int currentDestinationIndex) {
		this.currentDestinationIndex = currentDestinationIndex;
	}
	
	private Destination deletedDestination;
	private Destination editedDestination;
	
	public Destination getEditedDestination() {
		return editedDestination;
	}

	public void setEditedDestination(Destination editedDestination) {
		this.editedDestination = editedDestination;
	}

	private int page;

	public void remove(){
		destinationList = null;
Long id = deletedDestination.getId();
destinationResource.deleteDestination(id);		
	}
	
	public void store(){
		
		destinationList = null;
		destinationResource.updateDestination(editedDestination);
	}
	
	
	
	
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}


	public Destination getDeletedDestination() {
		return deletedDestination;
	}

	public void setDeletedDestination(Destination deletedDestination) {
		this.deletedDestination = deletedDestination;
	}

	
	
	
	
	
	
	
	
    public void resetValues() {
        // reset input fields to prevent stuck values after a validation failure
        // not necessary in JSF 2.2+ (@resetValues on a4j:commandButton)
        if (!JsfVersion.getCurrent().isCompliantWith(JsfVersion.JSF_2_2)) {
            FacesContext fc = FacesContext.getCurrentInstance();
            UIComponent comp = fc.getViewRoot().findComponent("form:editGrid");
 
            ((EditableValueHolder) comp.findComponent("form:name")).resetValue();
            ((EditableValueHolder) comp.findComponent("form:lat")).resetValue();
            ((EditableValueHolder) comp.findComponent("form:lon")).resetValue();
        }
    }
	
	
	
	
}